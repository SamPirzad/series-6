package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import java.math.RoundingMode;

public class Calculator implements Callable<BigDecimal> {

    private static BigDecimal One  = new BigDecimal(1) ;
    private static BigDecimal Four = new BigDecimal(4) ;
    private static BigDecimal Minus$One = new BigDecimal(-1) ;
    private static BigDecimal Minus$Two = new BigDecimal(-2) ;

    int Value ;

    public Calculator(int Value)
    {
        this.Value = Value;
    }

    public BigDecimal call()
    {
        BigDecimal fraction1 = Four.divide(new BigDecimal(8 * Value + 1) , 1000 , RoundingMode.HALF_EVEN) ;
        BigDecimal fraction2 = Minus$Two.divide(new BigDecimal(8 * Value + 4) , 1000 , RoundingMode.HALF_EVEN);
        BigDecimal fraction3 = Minus$One.divide(new BigDecimal(8 * Value + 5) , 1000 , RoundingMode.HALF_EVEN);
        BigDecimal fraction4 = Minus$One.divide(new BigDecimal(8 * Value + 6) , 1000 , RoundingMode.HALF_EVEN);

        BigDecimal sum = fraction1.add(fraction2).add(fraction3).add(fraction4) ;
        BigDecimal divider = new BigDecimal(16).pow(Value) ;
        BigDecimal fraction5 = One.divide(divider , 1000 , RoundingMode.HALF_EVEN) ;

        return fraction5.multiply(sum) ;
    }
}
