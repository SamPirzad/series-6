package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread
{

    private static final String MESSAGE = "hi blues, hi whites";
    private CountDownLatch CDL_BeforeBlue;

    public BlackThread(CountDownLatch latchBeforeBlue)
    {
        this.CDL_BeforeBlue = latchBeforeBlue;
    }

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run()
    {
        printMessage();
        CDL_BeforeBlue.countDown();
    }
}
