package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Runner {

    public static List<Message> messages = new ArrayList<>();

    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException
    {
        List<ColorThread> colorThreads = new ArrayList<>();
        CountDownLatch CTL_BeforeBlue = new CountDownLatch(blackCount);
        CountDownLatch CTL_BeforeWhite = new CountDownLatch(blueCount);
        CountDownLatch CTL_BeforeEnd = new CountDownLatch(whiteCount);

        for (int i = 0; i < blackCount; i++)
        {
            BlackThread blackThread = new BlackThread(CTL_BeforeBlue);
            colorThreads.add(blackThread);
            blackThread.start();
        }
        CTL_BeforeBlue.await();

        for (int i = 0; i < blueCount; i++)
        {
            BlueThread blueThread = new BlueThread(CTL_BeforeWhite);
            colorThreads.add(blueThread);
            blueThread.start();
        }
        CTL_BeforeWhite.await();

        for (int i = 0; i < whiteCount; i++)
        {
            WhiteThread whiteThread = new WhiteThread(CTL_BeforeEnd);
            colorThreads.add(whiteThread);
            whiteThread.start();
        }
        CTL_BeforeEnd.await();
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException
    {

    }
}
