package sbu.cs.exception;

import java.io.IOException;

public class ApException extends RuntimeException {
    private String message;

    public String getMessage()
    {
        return this.message;
    }
    public ApException(String message)
    {
        super(message);
        this.message = message;
    }
}


